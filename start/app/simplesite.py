from flask import Flask
import socket
app = Flask(__name__)
title = "ET 555"
lbl = socket.gethostname()
print(lbl)
@app.route('/')
def home():
    return f"<h1> {title} </h1><p>Hostname: {lbl} </p>"

if __name__ ==  '__main__':
    app.run(host='0.0.0.0', port=8080)
